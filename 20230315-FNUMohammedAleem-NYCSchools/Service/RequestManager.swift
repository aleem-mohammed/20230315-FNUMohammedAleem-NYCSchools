//
//  RequestManager.swift
//  20230315-FNUMohammedAleem-NYCSchools
//
//  Created by Aleem on 15/03/23.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case invalidReponse
    case invalidData
    
    var alertTitle: String {
        switch self {
        case .invalidURL:
            return "Invalid URL"
        case .invalidReponse:
            return "Server Error"
        case .invalidData:
            return "Data Error"
        }
    }
    
    var alertMessage: String {
        switch self {
        case .invalidURL:
            return "Provided URL is not in the correct format"
        case .invalidReponse:
            return "Something went wrong. Please try again later."
        case .invalidData:
            return "Received data is not in the correct format"
        }
    }
}

typealias ServerResult<T> = Result<T, NetworkError>

struct RequestManager {
    private init() { }
    static let shared = RequestManager()
    private var appToken = "N8JvRk25XASzapkeEx02FI11D"
    
    func getData<T: Codable>(url: String) async throws -> ServerResult<T> {
        guard let url = URL(string: url) else {
            return Result.failure(NetworkError.invalidURL)
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.addValue("Accept", forHTTPHeaderField: "application/json")
        urlRequest.addValue("X-App-Token", forHTTPHeaderField: appToken)
        
        let (data, response) = try await URLSession.shared.data(for: urlRequest)
        guard (response as? HTTPURLResponse)?.statusCode == 200 else {
            return Result.failure(NetworkError.invalidReponse)
        }
        
        do {
            let result = try JSONDecoder().decode(T.self, from: data)
            return Result.success(result)
        } catch {
            return Result.failure(NetworkError.invalidData)
        }
    }
}
