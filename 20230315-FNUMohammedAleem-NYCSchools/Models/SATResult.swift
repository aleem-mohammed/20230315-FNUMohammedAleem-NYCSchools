//
//  SATResult.swift
//  20230315-FNUMohammedAleem-NYCSchools
//
//  Created by Aleem on 14/03/23.
//

import Foundation

struct SATResult: Codable {
    var dbn: String?
    var schoolName: String?
    var reading: String?
    var math: String?
    var writing: String?
    var testTakers: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case reading = "sat_critical_reading_avg_score"
        case math = "sat_math_avg_score"
        case writing = "sat_writing_avg_score"
        case testTakers = "num_of_sat_test_takers"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.dbn = try container.decodeIfPresent(String.self, forKey: .dbn)
        self.schoolName = try container.decodeIfPresent(String.self, forKey: .schoolName)
        self.reading = try container.decodeIfPresent(String.self, forKey: .reading)
        self.math = try container.decodeIfPresent(String.self, forKey: .math)
        self.writing = try container.decodeIfPresent(String.self, forKey: .writing)
        self.testTakers = try container.decodeIfPresent(String.self, forKey: .testTakers)
    }
}
