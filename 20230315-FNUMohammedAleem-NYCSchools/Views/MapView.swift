//
//  MapView.swift
//  20230315-FNUMohammedAleem-NYCSchools
//
//  Created by Aleem on 15/03/23.
//

import SwiftUI
import MapKit

struct Location: Identifiable {
    var id = UUID()
    var coordinate: CLLocationCoordinate2D
}

// TODO: Include directions, Jump to marker
struct MapView: View {
    let latitude: Double
    let longitude: Double
    
    private var mapRegion: Binding<MKCoordinateRegion> {
        Binding (
            get: {
                MKCoordinateRegion(
                    center: CLLocationCoordinate2D(latitude: latitude, longitude: longitude),
                    span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
                )
            },
            set: { _ in }
        )
    }
    
    private var annotationItems: [Location] {
        [Location(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))]
    }
    
    var body: some View {
        Map(coordinateRegion: mapRegion, annotationItems: annotationItems) { location in
            MapMarker(coordinate: location.coordinate)
        }
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(latitude: 40.82746, longitude: -73.919)
    }
}
