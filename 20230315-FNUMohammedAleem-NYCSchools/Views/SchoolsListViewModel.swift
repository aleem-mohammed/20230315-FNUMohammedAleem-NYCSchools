//
//  SchoolsListViewModel.swift
//  20230315-FNUMohammedAleem-NYCSchools
//
//  Created by Aleem on 15/03/23.
//

import Foundation
import SwiftUI

@MainActor
class SchoolsListViewModel: ObservableObject {
    
    var schools: [School] = []
    var satResults: [SATResult] = []
    @Published var filteredSchools: [School] = []
    @Published var searchTerm: String = ""
    @Published var loading = false
    @Published var showAlert = false
    @Published var alertTitle = ""
    @Published var alertMessage = ""
    
    var errorAlert: Alert {
        Alert(
            title: Text(alertTitle),
            message: Text(alertMessage),
            primaryButton: .default(
                Text("Try Again"),
                action: {
                    Task {
                        self.resetAlertData()
                        await self.getSchoolsData()
                        await self.fetchSATResults()
                    }
                }
            ),
            secondaryButton: .cancel(
                Text("Dismiss"),
                action: {
                    self.resetAlertData()
                }
            )
        )
    }
    
    func satResult(dbn: String) -> SATResult? {
        satResults.first(where: { $0.dbn == dbn })
    }
    
    // TODO: Need to refine search logic. Currently we are appling filter to only those fields that are shown in the list
    func applyFilter(searchTerm: String) {
        guard !searchTerm.isEmpty else {
            filteredSchools = schools
            return
        }
        filteredSchools = schools.filter {
            $0.name?.contains(searchTerm) ?? false ||
            $0.addressLine1?.contains(searchTerm) ?? false ||
            $0.neighborhood?.contains(searchTerm) ?? false ||
            $0.city?.contains(searchTerm) ?? false
        }
    }
    
    func updateAlertData(error: NetworkError) {
        alertTitle = error.alertTitle
        alertMessage = error.alertMessage
        showAlert = true
    }
    
    func updateAlertData(error: Error) {
        alertTitle = ""
        alertMessage = error.localizedDescription
        showAlert = true
    }
    
    func resetAlertData() {
        alertTitle = ""
        alertMessage = ""
        showAlert = false
    }
}

extension SchoolsListViewModel {
    func getSchoolsData() async {
        do {
            loading = true
            let result = try await SchoolService.shared.getSchoolsData()
            loading = false
            switch result {
            case .success(let schools):
                self.schools = schools
                self.filteredSchools = schools
            case .failure(let error):
                updateAlertData(error: error)
            }
        } catch {
            loading = false
            updateAlertData(error: error)
        }
    }
    
    func fetchSATResults() async {
        do {
            loading = true
            let result = try await SchoolService.shared.getSATResults()
            loading = false
            switch result {
            case .success(let satResults):
                self.satResults = satResults
            case .failure(let error):
                updateAlertData(error: error)
            }
        } catch {
            loading = false
            updateAlertData(error: error)
        }
    }
}
