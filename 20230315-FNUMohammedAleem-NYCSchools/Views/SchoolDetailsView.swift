//
//  SchoolDetailsView.swift
//  20230315-FNUMohammedAleem-NYCSchools
//
//  Created by Aleem on 14/03/23.
//

import SwiftUI

// TODO: Add more sections for prgdesc, directions, requirement, offer_rate, program, interest, method, admissionspriority, eligibility, auditioninformation, common_audition.
// TODO: If the section content is more show in another screen.
struct SchoolDetailsView: View {
    let school: School
    let satResult: SATResult
    
    var body: some View {
        List {
            Section(
                content: { Text(school.overview ?? "") },
                header: { Text("Overview") }
            )
            
            Section(
                content: {
                    VStack {
                        LabeledContent("No of Test Takers", value: satResult.testTakers ?? "")
                        LabeledContent("Avg Math Score", value: satResult.math ?? "")
                        LabeledContent("Avg Critical Reading Score", value: satResult.reading ?? "")
                        LabeledContent("Avg Writing Score", value: satResult.writing ?? "")
                    }
                }, header: {
                    Text("SAT Stats")
                }
            )
            
            Section(
                content: {
                    VStack(alignment: .leading) {
                        if let opportunities = school.academicOpportunities1 {
                            Text(opportunities)
                        }
                        if let opportunities = school.academicOpportunities2 {
                            Text(opportunities)
                                .padding(.top)
                        }
                        if let opportunities = school.academicOpportunities3 {
                            Text(opportunities)
                                .padding(.top)
                        }
                        if let opportunities = school.academicOpportunities4 {
                            Text(opportunities)
                                .padding(.top)
                        }
                        if let opportunities = school.academicOpportunities5 {
                            Text(opportunities)
                                .padding(.top)
                        }
                    }
                }, header: {
                    Text("Academic Opportunities")
                }
            )
            
            if let extracurricularActivities = school.extracurricularActivities {
                Section(
                    content: {
                        Text(extracurricularActivities)
                    }, header: {
                        Text("Extracurricular Activities")
                    }
                )
            }
            
            Section(
                content: {
                    VStack(alignment: .leading) {
                        LabeledContent("Phone", value: school.phone ?? "")
                        LabeledContent("Email", value: school.email ?? "")
                    }
                }, header: {
                    Text("Contact Info")
                }
            )
            
            if let latitude = Double(school.latitude ?? ""), let longitude = Double(school.longitude ?? "" ) {
                Section(
                    content: {
                        MapView(latitude: latitude, longitude: longitude)
                            .frame(height: 200)
                            .listRowInsets(EdgeInsets())

                    }, header: {
                        Text("Location")
                    }
                )
            }
        }
        .navigationTitle(school.name ?? "")
    }
}
