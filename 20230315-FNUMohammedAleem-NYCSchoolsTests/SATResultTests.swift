//
//  SATResultTests.swift
//  20230315-FNUMohammedAleem-NYCSchoolsTests
//
//  Created by Aleem on 15/03/23.
//

import XCTest
@testable import _0230315_FNUMohammedAleem_NYCSchools

final class SATResultTests: XCTestCase {
    func testJSONMapping() throws {
        let bundle = Bundle(for: type(of: self))
        
        guard let url = bundle.url(forResource: "SATResult", withExtension: "json") else {
            XCTFail("Missing file: SATResult.json")
            return
        }
        
        let jsonData = try Data(contentsOf: url)
        
        let satResult = try JSONDecoder().decode(SATResult.self, from: jsonData)
        XCTAssertEqual(satResult.dbn, "01M292")
        XCTAssertEqual(satResult.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(satResult.reading, "355")
        XCTAssertEqual(satResult.math, "404")
        XCTAssertEqual(satResult.writing, "363")
        XCTAssertEqual(satResult.testTakers, "29")
    }
}
