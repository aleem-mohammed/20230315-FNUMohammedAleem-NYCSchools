//
//  SchoolServiceTests.swift
//  20230315-FNUMohammedAleem-NYCSchoolsTests
//
//  Created by Aleem on 15/03/23.
//

import XCTest
@testable import _0230315_FNUMohammedAleem_NYCSchools

final class SchoolServiceTests: XCTestCase {
    
    func testGetSchoolsData() throws {
        let expectation = XCTestExpectation(description: "Fetch NYC Schools data")
        Task {
            do {
                let result = try await SchoolService.shared.getSchoolsData()
                switch result {
                case .success(let schools):
                    XCTAssertNotNil(schools)
                    expectation.fulfill()
                case .failure(let error):
                    XCTAssertNotNil(error)
                    expectation.fulfill()
                }
            } catch {
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 4)
    }
    
    func testGetSATResults() throws {
        let expectation = XCTestExpectation(description: "Fetch SAT Results")
        Task {
            do {
                let result = try await SchoolService.shared.getSATResults()
                switch result {
                case .success(let satResults):
                    XCTAssertNotNil(satResults)
                    expectation.fulfill()
                case .failure(let error):
                    XCTAssertNotNil(error)
                    expectation.fulfill()
                }
            } catch {
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 4)
    }
}
