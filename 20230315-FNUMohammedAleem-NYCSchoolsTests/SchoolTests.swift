//
//  SchoolTests.swift
//  20230315-FNUMohammedAleem-NYCSchoolsTests
//
//  Created by Aleem on 15/03/23.
//

import XCTest
@testable import _0230315_FNUMohammedAleem_NYCSchools

final class SchoolTests: XCTestCase {
    func testJSONMapping() throws {
        let bundle = Bundle(for: type(of: self))
        
        guard let url = bundle.url(forResource: "School", withExtension: "json") else {
            XCTFail("Missing file: School.json")
            return
        }
        
        let jsonData = try Data(contentsOf: url)
        
        let school = try JSONDecoder().decode(School.self, from: jsonData)
        XCTAssertEqual(school.dbn, "09X505")
        XCTAssertEqual(school.name, "Bronx School for Law, Government and Justice")
        XCTAssertEqual(school.addressLine1, "244 East 163rd Street")
        XCTAssertEqual(school.neighborhood, "E. Concourse-Concourse Village")
        XCTAssertEqual(school.city, "Bronx")
        XCTAssertEqual(school.state, "NY")
        XCTAssertEqual(school.phone, "718-410-3430")
        XCTAssertNil(school.email)
    }
}
